//
//  HPMChooser.m
//
//  Created by Carlos Sola-Llonch on 6/9/14.
//
//

#import "HPMChooser.h"
#import <Cordova/CDVPluginResult.h>

@implementation HPMChooser {
    id _callbackId;
}

- (void)pluginInitialize
{
    [super pluginInitialize];
    if ([self.viewController conformsToProtocol:@protocol(HPMChooserDelegate)]) {
        self.delegate = (id<HPMChooserDelegate>)self.viewController;
    }
}

- (void)present:(CDVInvokedUrlCommand*)command
{
    if ([@"present" caseInsensitiveCompare:command.methodName] == NSOrderedSame) {
        _callbackId = command.callbackId;
        [self.delegate initializeWithPlugin:self];
        [self.delegate presentUIWithArguments:command.arguments];
    }
}

- (void)sendResponse:(NSInteger)response {
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:response];
    [self success:result callbackId:_callbackId];
}

@end
