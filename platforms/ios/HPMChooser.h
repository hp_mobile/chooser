//
//  HPMChooser.h
//
//  Created by Carlos Sola-Llonch on 6/9/14.
//
//

#import <Cordova/CDVPlugin.h>

@class HPMChooser;

@protocol HPMChooserDelegate <NSObject>
- (void)initializeWithPlugin:(HPMChooser *)plugin;
- (void)presentUIWithArguments:(NSArray *)args;
@end

@interface HPMChooser : CDVPlugin

- (void)sendResponse:(NSInteger)response;

@property (nonatomic, weak) id<HPMChooserDelegate> delegate;

@end

