package com.highpointmobile.cordova;

import java.util.ArrayList;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;

public class Chooser extends CordovaPlugin {
	private CallbackContext mCallbackContext;
	
	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		if ("present".equals(action)) {
			if (!(this.cordova.getActivity() instanceof ChooserListener))
				return false;

			final ArrayList<String> arguments = new ArrayList<String>();
			for(int a = 0; a < args.length(); ++a) {
				arguments.add(args.getString(a));
			}
			
			mCallbackContext = callbackContext;
			
			final Activity mainActivity = cordova.getActivity();
			// remember this is running in the webcore thread.
			mainActivity.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					ChooserListener listener = (ChooserListener)mainActivity;
					listener.initialize(Chooser.this);
					listener.presentDialog(arguments);
				}
				
			});
			return true;
		}
		
		return false;
	}
	
	public void sendResponse(int index) {
		PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, index);
		mCallbackContext.sendPluginResult(pluginResult);
	}
}
