package com.highpointmobile.cordova;

import java.util.List;

public interface ChooserListener {
	public void initialize(Chooser plugin);
	public void presentDialog(List<String> args);
}
