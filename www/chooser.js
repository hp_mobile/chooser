if (!window.plugins) {
    window.plugins = {};
}

var ChooserWrapper = {
    actions : [],

    imageElementName : "com-highpointmobile-img_button",

    reset : function() {
        this.actions = [];
    },

    on : function(that, index) {
        var fn = this.actions[index].callback;
        var params = this.actions[index].params;
        if (params === null)
            fn.call(that, null);
        if (typeof params.push === "function")
            fn.apply(that, params);
        else
            fn.call(that, params);
        this.reset();
    },

    chooser : {
        // call this to clear out any previous state
        init : function() {
            ChooserWrapper.actions = [];
            return this;
        },

        // Pass multiple params as an array. In the callback, they array will be unrolled
        add_action : function(description, func, params) {
            ChooserWrapper.actions.push({
                action_label : description,
                callback     : func,
                params       : params
            });

            return this;
        },

        show : function() {
            var args = [];
            for (var i = 0; i < ChooserWrapper.actions.length; ++i) {
                args.push(ChooserWrapper.actions[i].action_label);
            };

            var self = this;
            cordova.exec(
                function(index) {
                    ChooserWrapper.on(self, index);
                },
                function(error) {

                },
                "Chooser",
                "present",
                args
            );
        },
        
        imageElementRect : function() {
            var el = document.getElementById(ChooserWrapper.imageElementName);
            var rect = el.getBoundingClientRect();

            return rect.left + "," + rect.top + "," + rect.width + "," + rect.height;
        }
    }
}

module.exports = ChooserWrapper.chooser;
//window.chooser = ChooserWrapper.chooser;

